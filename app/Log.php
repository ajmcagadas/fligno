<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    public $table = 'logs';

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'table',
        'column_name',
        'row_id',
        'old_value',
        'new_value',
        'added_on',
        'method'
    ];
}
