<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = [
        'item_id',
        'item_name',
        'stock_id',
        'service_',
        'number_of_items_sold',
        'amount'
    ];

    // protected $dateFormat = 'Y-m-d H:i:sO';

    public function stock() {
        return $this->belongsTo('App\Stock');
    }

    public function service() {
        return $this->belongsTo('App\Service');
    }
}
