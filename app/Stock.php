<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = [
        'item_id',
        'store_name',
        'number_of_items',
        'total_count',
        'amount',
        'price_per_item',
        'sold_count'
    ];

    // protected $dateFormat = 'Y-m-d H:i:sO';

    public function item() {
        return $this->belongsTo('App\Item');
    }

    public function sales() {
        return $this->hasMany('App\Sale');
    }
}
