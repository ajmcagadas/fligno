<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    // protected $dateFormat = 'Y-m-d H:i:sO';

    protected $fillable = [
        'name',
        'price',
        'status'
    ];

    public function sales() {
        return $this->hasMany('App\Sale');
    }

}
