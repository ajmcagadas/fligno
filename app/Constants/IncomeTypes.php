<?php

namespace App\Constants;

class IncomeTypes {
    const PRODUCT = "PRODUCT";
    const SERVICE = "SERVICE";
}
