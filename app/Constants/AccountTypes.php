<?php

namespace App\Constants;

class AccountTypes {
    const ADMIN = "ADMIN";
    const NORMAL = "NORMAL";
}
