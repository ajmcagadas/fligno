<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = [
            'name',
            'description',
            'status'
    ];

    // protected $dateFormat = 'Y-m-d H:i:sO';

    public function stocks() {
        return $this->hasMany('App\Stock');
    }
}
