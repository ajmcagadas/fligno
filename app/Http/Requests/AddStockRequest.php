<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddStockRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_id' => 'required|numeric',
            'store_name' => 'required|max:200',
            'number_of_items' => 'required',
            'amount' => 'required|numeric',
            'price_per_item' => 'required|numeric'
        ];
    }
}
