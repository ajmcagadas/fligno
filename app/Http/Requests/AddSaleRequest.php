<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Constants\IncomeTypes;

class AddSaleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service_id' => 'nullable|numeric',
            'item_id' => 'required|numeric',
            'number_of_items_sold' => 'required|numeric',
            'income_type' => 'required|in:'. IncomeTypes::PRODUCT . ',' . IncomeTypes::SERVICE,
            'service_charge' => 'nullable|numeric'
        ];
    }
}
