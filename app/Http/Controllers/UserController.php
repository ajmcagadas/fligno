<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function get() {
        return response()->json([
            'success' => true,
            'data' => User::select('id', 'name', 'email', 'account_type')->get()
        ]);
    }
}
