<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Http\Requests\AddSaleRequest;
use App\Stock;
use App\Sale;
use App\Service;
use Illuminate\Support\Facades\DB;
use App\Constants\IncomeTypes;
use Illuminate\Support\Facades\Cache;
use Carbon\Carbon;

class SaleController extends Controller
{
    /**
     * @TODO: Audit
     */



     /**
      * Get list of sales for the day
      *
      * @return void
      */
    public function get() {
        return response()->json([
            'success' => true,
            'data' => Sale::whereRaw('created_at::date = NOW()::date')->get()
        ]);
    }

    /**
     * Add transaction into sales table
     * This will record both item and service sale
     *
     * @param AddSaleRequest $request
     * @return json
     */
    public function add(AddSaleRequest $request) {

        $incomeType = $request->income_type;

        $stock = Stock::select('id', 'total_count', 'sold_count', 'item_id', 'price_per_item')
            ->where('item_id', $request->item_id)
            ->where('total_count', '>', 0)
            ->orderBy('created_at', 'ASC');

        /** Cehck for stock */
        if($stock->count() <= 0) {
            return response()->json([
                'success' => false,
                "data" => "Item of stock"
            ]);
        }

        $stock = $stock->first();

        $item = Item::select('name')
            ->where('id', $request->item_id)
            ->first();

        /** Check for the type of income */
        if ($incomeType === IncomeTypes::SERVICE) {
            $service = Service::where('id', $request->service_id)->first();
            $serviceCharge = $request->service_charge ?: $service->price;
        }


        /**
         * Item has a stock but check if number of items
         * is less than the needed items to be used
         * */
        if ($stock->total_count < $request->number_of_items_sold) {

            $stock2 = Stock::select('id', 'total_count', 'sold_count', 'item_id', 'price_per_item')
                ->where('item_id', $request->item_id)
                ->whereNotIn('id', [$stock->id])
                ->where('total_count', '>', 0)
                ->orderBy('created_at', 'DESC');

            if($stock2->count() <= 0) {
                return response()->json([
                        'success' => false,
                        'data' => 'Stock not enough'
                    ]);
            }

            $stock2 = $stock2->first();

            $itemsLeft = $request->number_of_items_sold - $stock->total_count;

            // check for per item price difference
            if($stock2->price_per_item != $stock->price_per_item) {
                return response()->json([
                    'success' => false,
                    'data' => 'The stocks available have different per item price'
                ]);
            }

            Sale::insert([
                [
                    'stock_id'  =>  $stock->id,
                    'item_name' => $item->name,
                    'number_of_items_sold' => $stock->total_count,
                    'amount' => $stock->price_per_item * $stock->total_count
                ],
                [
                    'stock_id'  =>  $stock2->id,
                    'item_name' => $item->name,
                    'number_of_items_sold' => $itemsLeft,
                    'amount' => $stock2->price_per_item * $itemsLeft
                ]
            ]);

            $totalItemSale = ($stock->price_per_item * $stock->total_count) + ($stock2->price_per_item * $itemsLeft);

            Stock::where('id', $stock->id)
                ->update([
                    'total_count' => 0,
                    'sold_count' => $stock->total_count + $stock->sold_count
                ]);

            Stock::where('id', $stock2->id)
                ->update([
                    'total_count' => $stock2->total_count - $itemsLeft,
                    'sold_count' => $stock2->sold_count + ($request->number_of_items_sold - $stock->total_count)
                ]);


            if ($incomeType === IncomeTypes::SERVICE) {
                //$totalItemSale
                Sale::create([
                    'service_id'  =>  $request->service_id,
                    'item_name' => $service->name,
                    'number_of_items_sold' => 1,
                    'amount' => $serviceCharge - $totalItemSale
                ]);
            }

        } else {

            if($incomeType === IncomeTypes::SERVICE) {

                //check if item used in service was wasted
                $itemPrice  = $service->price < 0
                    ? $stock->price_per_item * $request->number_of_items_sold * -1
                    : $stock->price_per_item * $request->number_of_items_sold;

                //$itemPrice  = $stock->price_per_item * $request->number_of_items_sold;
                $serviceCharge = $service->price > 0
                    ? $request->service_charge - $itemPrice
                    : 0;

                $payload = [
                    [
                        'stock_id'  =>  $stock->id,
                        'service_id'  =>  NULL,
                        'item_name' => $item->name,
                        'number_of_items_sold' => $request->number_of_items_sold,
                        'amount' => $itemPrice,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ],
                    [
                        'stock_id'  =>  NULL,
                        'service_id'  =>  $service->id,
                        'item_name' => $service->name,
                        'number_of_items_sold' => $request->number_of_items_sold,
                        'amount' => $serviceCharge,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ]
                ];

                Sale::insert($payload);

            } else {
                $payload = [
                    'stock_id'  =>  $stock->id,
                    'item_name' => $item->name,
                    'number_of_items_sold' => $request->number_of_items_sold,
                    'amount' => $stock->price_per_item * $request->number_of_items_sold
                ];
                Sale::create($payload);
            }

            Stock::where('id', $stock->id)
                ->update([
                    'total_count' => $stock->total_count - $request->number_of_items_sold,
                    'sold_count' => $stock->sold_count + $request->number_of_items_sold
                ]);
        }

        return response()->json([
            'success' => true
        ]);


    }
}
