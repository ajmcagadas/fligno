<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Stock;
use App\Log as Audit;
use App\Http\Requests\AddItemRequest;
use App\Http\Requests\AddStockRequest;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\IdOnlyRequest;

class StockController extends Controller
{
    /**
     * Get item information and stocks
     *
     * @param int $itemId
     * @return void
     */
    public function get($itemId) {
        return response()->json([
            'success' => true,
            'data' => Item::where('id', $itemId)
                            ->with(['stocks' => function($query) {
                                $query->orderBy('created_at', 'ASC');
                            }])->first()
        ]);
    }

    public function add(AddStockRequest $request) {

        $payload = [
            'item_id' => $request->item_id,
            'store_name' => $request->store_name,
            'number_of_items' => $request->number_of_items,
            'total_count' => $request->number_of_items,
            'price_per_item' => $request->price_per_item,
            'amount' => $request->amount
        ];

        $addStock = Stock::create($payload);

        $logs = [];
        foreach($payload as $key => $item) {
            $logs[] = [
                'user_id' => Auth::id(),
                'table' => 'stocks',
                'action_name' =>  'INSERT',
                'column_name' => $key,
                'row_id' => $addStock->id,
                'new_value' => $item
            ];
        }

        Audit::insert($logs);

        return response()->json([
            'success' => $addStock ? true : false,
            'data' => [
                'id' => $addStock->id
            ]
        ]);
    }

    public function delete(IdOnlyRequest $request) {

        $stockQuery = Stock::where('id', $request->id)
                        ->whereRaw('number_of_items = total_count');

        $stockInfo = $stockQuery->first();
        $delete = $stockQuery->delete();

        if ($delete) {
            Audit::insert([
                [
                    'user_id' => Auth::id(),
                    'table' => 'stocks',
                    'action_name' =>  'DELETE',
                    'column_name' => 'item_id',
                    'row_id' => $request->id,
                    'old_value' => $stockInfo->item_id,
                    'new_value' => NULL
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'stocks',
                    'action_name' =>  'DELETE',
                    'column_name' => 'store_name',
                    'row_id' => $request->id,
                    'old_value' => $stockInfo->store_name,
                    'new_value' => NULL
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'stocks',
                    'action_name' =>  'DELETE',
                    'column_name' => 'price_per_item',
                    'row_id' => $request->id,
                    'old_value' => $stockInfo->price_per_item,
                    'new_value' => NULL
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'stocks',
                    'action_name' =>  'DELETE',
                    'column_name' => 'number_of_items',
                    'row_id' => $request->id,
                    'old_value' => $stockInfo->number_of_items,
                    'new_value' => NULL
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'stocks',
                    'action_name' =>  'DELETE',
                    'column_name' => 'total_count',
                    'row_id' => $request->id,
                    'old_value' => $stockInfo->total_count,
                    'new_value' => NULL
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'stocks',
                    'action_name' =>  'DELETE',
                    'column_name' => 'sold_count',
                    'row_id' => $request->id,
                    'old_value' => $stockInfo->sold_count,
                    'new_value' => NULL
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'stocks',
                    'action_name' =>  'DELETE',
                    'column_name' => 'amount',
                    'row_id' => $request->id,
                    'old_value' => $stockInfo->amount,
                    'new_value' => NULL
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'stocks',
                    'action_name' =>  'DELETE',
                    'column_name' => 'created_at',
                    'row_id' => $request->id,
                    'old_value' => $stockInfo->created_at,
                    'new_value' => NULL
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'stocks',
                    'action_name' =>  'DELETE',
                    'column_name' => 'updated_at',
                    'row_id' => $request->id,
                    'old_value' => $stockInfo->updated_at,
                    'new_value' => NULL
                ]
            ]);
            return response()->json([
                    'success' => true
                ]);
        }

        return response()->json([
                    'success' => false,
                    'data' => 'Deletion not allowed'
                ]);
    }
}
