<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Log as Audit;
use App\Http\Requests\NewItemRequest;
use App\Http\Requests\IdOnlyRequest;
use App\Http\Requests\ItemUpdateRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ItemController extends Controller
{
    /**
     * Fetch all items
     *
     * @return json
     */
    public function get() {
        return response()->json([
            'data' => Item::select('id', 'name', 'description', 'status')
                        ->get()
        ]);
    }

    /**
     * Add new Item
     *
     * @param NewItemRequest $request
     * @return json
     */
    public function add(NewItemRequest $request) {
        $payload = [
            'name' => $request->name,
            'description'=> $request->description
        ];

        $addItem = Item::create($payload);

        $logs = [];
        foreach($payload as $key => $item) {
            $logs[] = [
                'user_id' => Auth::id(),
                'table' => 'items',
                'action_name' =>  'INSERT',
                'column_name' => $key,
                'row_id' => $addItem->id,
                'new_value' => $item
            ];
        }

        Audit::insert($logs);

        return response()->json([
            'success' => $addItem ? true : false,
            'data' => [
                'id' => $addItem->id
            ]
        ]);
    }

    /**
     * Delete an item
     *
     * @return json
     */
    public function delete(IdOnlyRequest $request) {

        $itemQuery = Item::where('id', $request->id);
        $itemInfo = $itemQuery->first();
        $removeItem = $itemQuery->delete();

        if ($removeItem) {
            $logs = [
                [
                    'user_id' => Auth::id(),
                    'table' => 'items',
                    'action_name' =>  'DELETE',
                    'column_name' => "name",
                    'row_id' => $request->id,
                    'old_value' => $itemInfo->name,
                    'new_value' => NULL
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'items',
                    'action_name' =>  'DELETE',
                    'column_name' => "description",
                    'row_id' => $request->id,
                    'old_value' => $itemInfo->description,
                    'new_value' => NULL
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'items',
                    'action_name' =>  'DELETE',
                    'column_name' => "status",
                    'row_id' => $request->id,
                    'old_value' => $itemInfo->status,
                    'new_value' => NULL
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'items',
                    'action_name' =>  'DELETE',
                    'column_name' => "created_at",
                    'row_id' => $request->id,
                    'old_value' => $itemInfo->created_at,
                    'new_value' => NULL
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'items',
                    'action_name' =>  'DELETE',
                    'column_name' => "updated_at",
                    'row_id' => $request->id,
                    'old_value' => $itemInfo->updated_at,
                    'new_value' => NULL
                ]
            ];

            Audit::insert($logs);

            return response()->json([
                    'success' => true
                ]);
        }

        return response()->json([
                'success' => false
            ]);

    }

    /**
     * Update item status
     *
     * @return json
     */
    public function updateStatus(IdOnlyRequest $request) {

        $itemQuery = Item::where('id', $request->id);

        $itemInfo = $itemQuery->first();
        $update = $itemQuery->update([
            'status' => DB::raw("NOT status")
        ]);

        if ($update) {

            Audit::insert([
                [
                    'user_id' => Auth::id(),
                    'table' => 'items',
                    'action_name' =>  'UPDATE',
                    'column_name' => "status",
                    'row_id' => $request->id,
                    'old_value' => $itemInfo->status,
                    'new_value' => !$itemInfo->status
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'items',
                    'action_name' =>  'UPDATE',
                    'column_name' => "updated_at",
                    'row_id' => $request->id,
                    'old_value' => $itemInfo->updated_at,
                    'new_value' => now()
                ],
            ]);

            return response()->json([
                    'success' => true,
                    'data' => [
                        'status' => !$itemInfo->status
                    ]
                ]);

        }

        return response()->json([
                'success' => false
            ]);

    }

    /**
     * Update Item details
     *
     * * @return json
     */
    public function updateDetails(ItemUpdateRequest $request) {

        $itemQuery = Item::where('id', $request->id);

        $itemInfo = $itemQuery->first();
        $update = $itemQuery->update([
            'name' => $request->name,
            'description' => $request->description
        ]);

        if ($update) {
            Audit::insert([
                [
                    'user_id' => Auth::id(),
                    'table' => 'items',
                    'action_name' =>  'UPDATE',
                    'column_name' => "name",
                    'row_id' => $request->id,
                    'old_value' => $itemInfo->name,
                    'new_value' => $request->name
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'items',
                    'action_name' =>  'UPDATE',
                    'column_name' => "description",
                    'row_id' => $request->id,
                    'old_value' => $itemInfo->description,
                    'new_value' => $request->description
                ],
            ]);

            return response()->json([
                    'success' => true
                ]);
        }

        return response()->json([
                'success' => false
            ]);
    }

    /**
     * Get item information
     *
     * @return json
     */
    public function getItem($id) {

        return response()->json([
            'success' => true,
            'data' => Item::select('id', 'name', 'description', 'status')
                ->where('id', $id)
                ->first()
        ]);
    }
}
