<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AddServiceRequest;
use App\Http\Requests\IdOnlyRequest;
use App\Http\Requests\ServiceUpdateRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Auth;
use App\Log as Audit;
use App\Service as MyService;

class ServiceController extends Controller
{
    public function get() {
        return response()->json([
            'success' => true,
            'data' => MyService::select('id', 'name', 'price', 'status')->get()
        ]);
    }

    public function add(AddServiceRequest $request) {

        $payload = [
            'name' => $request->name,
            'price' => $request->price
        ];

        $addService = MyService::create($payload);

        if($addService) {
            $logs = [];
            foreach($payload as $key => $item) {
                $logs[] = [
                    'user_id' => Auth::id(),
                    'table' => 'services',
                    'action_name' =>  'INSERT',
                    'column_name' => $key,
                    'row_id' => $addService->id,
                    'new_value' => $item
                ];
            }
            Audit::insert($logs);

            return response()->json([
                'success' => true,
                'data' => [
                    'id' => $addService->id
                ]
            ]);
        }

        return response()->json([
                'success' => false
            ]);

    }

    public function delete(IdOnlyRequest $request) {
        $serviceQuery = MyService::where('id', $request->id);

        $serviceInfo = $serviceQuery->first();
        $delete = $serviceQuery->delete();

        if ($delete) {
            $logs = [
                [
                    'user_id' => Auth::id(),
                    'table' => 'services',
                    'action_name' =>  'DELETE',
                    'column_name' => 'name',
                    'row_id' => $request->id,
                    'new_value' => NULL,
                    'old_value' => $serviceInfo->name
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'services',
                    'action_name' =>  'DELETE',
                    'column_name' => 'price',
                    'row_id' => $request->id,
                    'new_value' => NULL,
                    'old_value' => $serviceInfo->price
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'services',
                    'action_name' =>  'DELETE',
                    'column_name' => 'status',
                    'row_id' => $request->id,
                    'new_value' => NULL,
                    'old_value' => $serviceInfo->status
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'services',
                    'action_name' =>  'DELETE',
                    'column_name' => 'created_at',
                    'row_id' => $request->id,
                    'new_value' => NULL,
                    'old_value' => $serviceInfo->created_at
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'services',
                    'action_name' =>  'DELETE',
                    'column_name' => 'updated_at',
                    'row_id' => $request->id,
                    'new_value' => NULL,
                    'old_value' => $serviceInfo->updated_at
                ]
            ];

            Audit::insert($logs);

            return response()->json([
                    'success' => true
                ]);

        }

        return response()->json([
                'success' => false
            ]);
    }

    /**
     * Update service status
     *
     * @return json
     */
    public function updateStatus(IdOnlyRequest $request) {

        $serviceQuery = MyService::where('id', $request->id);

        $serviceInfo = $serviceQuery->first();
        $update = $serviceQuery->update([
            'status' => DB::raw("NOT status")
        ]);

        if ($update) {

            Audit::insert([
                [
                    'user_id' => Auth::id(),
                    'table' => 'services',
                    'action_name' =>  'UPDATE',
                    'column_name' => "status",
                    'row_id' => $request->id,
                    'old_value' => $serviceInfo->status,
                    'new_value' => !$serviceInfo->status
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'items',
                    'action_name' =>  'UPDATE',
                    'column_name' => "updated_at",
                    'row_id' => $request->id,
                    'old_value' => $serviceInfo->updated_at,
                    'new_value' => now()
                ],
            ]);

            return response()->json([
                    'success' => true,
                    'data' => [
                        'status' => !$serviceInfo->status
                    ]
                ]);

        }

        return response()->json([
                'success' => false
            ]);

    }


     /**
     * Update Service details
     *
     * * @return json
     */
    public function updateDetails(ServiceUpdateRequest $request) {

        $serviceQuery = MyService::where('id', $request->id);

        $serviceInfo = $serviceQuery->first();
        $update = $serviceQuery->update([
            'name' => $request->name,
            'price' => $request->price
        ]);

        if ($update) {
            Audit::insert([
                [
                    'user_id' => Auth::id(),
                    'table' => 'services',
                    'action_name' =>  'UPDATE',
                    'column_name' => "name",
                    'row_id' => $request->id,
                    'old_value' => $serviceInfo->name,
                    'new_value' => $request->name
                ],
                [
                    'user_id' => Auth::id(),
                    'table' => 'services',
                    'action_name' =>  'UPDATE',
                    'column_name' => "price",
                    'row_id' => $request->id,
                    'old_value' => $serviceInfo->price,
                    'new_value' => $request->price
                ],
            ]);

            return response()->json([
                    'success' => true
                ]);
        }

        return response()->json([
                'success' => false
            ]);
    }











    public function statusUpdate(IdOnlyRequest $request) {
        $statusUpdate = MyService::where('id', $request->id)
            ->update([
                'status' => DB::raw('NOT status')
            ]);

        if ($statusUpdate) {
            Cache::put('services', '', 0);
            session()->flash('success', true);
            session()->flash('message', "Service successfully updated");
        } else {
            session()->flash('success', false);
            session()->flash('message', "Oh snap! Failed to update service");
        }

        return redirect()->route('service');
    }

    public function edit($id) {
        if(Cache::has('services')) {
            $data['services'] = Cache::get('services');
        } else {
            $data['services'] = MyService::all();
            Cache::put('services', $data['services']);
        }
        $data['service'] = MyService::where('id', $id)->first();
        return view('services.edit', $data);
    }

    public function put($id, AddServiceRequest $request) {
        $updateService = MyService::where('id', $id)->update([
            'name' => $request->name,
            'price' => $request->price
        ]);

        if ($updateService) {
            Cache::put('services', '', 0);
            session()->flash('success', true);
            session()->flash('message', "Service successfully updated");
        } else {
            session()->flash('success', false);
            session()->flash('message', "Oh snap! Failed to update service");
        }

        return redirect()->back();

    }
}
