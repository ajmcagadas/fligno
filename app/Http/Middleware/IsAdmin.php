<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Constants\AccountTypes;
class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user()->account_type !== AccountTypes::ADMIN) {
            return response()
                ->json([
                    'success' => false,
                    'data' => 'Forbidden access'
                ])->header('Status', 403);
        }
        return $next($request);
    }
}
