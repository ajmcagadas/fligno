<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->string('table');
            $table->string('action_name');
            $table->string('column_name');
            $table->string('row_id');
            $table->string('old_value')->nullable();
            $table->string('new_value')->nullable();
            $table->timestamp('added_on')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
