const salesMutations = {
    SET_SALES(state, payload) {
        state.sales = payload;
    },
    ADD_SALE(state, payload) {
        state.sales.unshift(payload);
    }
    // /** payload is the index & stock_ id of the element to be removed */
    // REMOVE_SERVICE(state, payload) {
    //     state.services.splice(payload, 1);
    // },
    // UPDATE_SERVICE_STATUS(state, payload) {
    //     console.log("UPDATE_SERVICE_STATUS:", payload);
    //     state.services[payload.index].status = payload.status;
    // },
    // UPDATE_SERVICE_DETAILS(state, payload) {
    //     console.log("UPDATE_SERVICE_DETAILS:", payload);
    //     state.services[payload.index].name = payload.name.value;
    //     state.services[payload.index].price = payload.price.value;
    // }
};

export default salesMutations;
