const itemMutations = {
    SET_STOCKS(state, payload) {
        state.stocks = payload;
    },
    ADD_STOCK(state, payload) {
        console.log("MUTATIONS PAYLOAD: ", payload);
        state.stocks[payload.item_id].unshift(payload);
    },
    /** payload is the index & stock_ id of the element to be removed */
    REMOVE_STOCK(state, payload) {
        state.stocks[payload.item_id].splice(payload.index, 1);
    }
};

export default itemMutations;
