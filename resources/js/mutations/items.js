const itemMutations = {
    SET_ITEMS(state, payload) {
        state.items = payload;
    },
    ADD_ITEM(state, payload) {
        state.items.push(payload);
    },
    /** payload is the index of the element to be removed */
    REMOVE_ITEM(state, payload) {
        state.items.splice(payload, 1);
    },
    UPDATE_ITEM_STATUS(state, payload) {
        // console.log("PAYLOAD:", payload);
        state.items[payload.index].status = payload.status;
    },
    UPDATE_ITEM_DETAILS(state, payload) {
        // console.log("UPDATE_ITEM_DETAILS_PAYLOAD:", payload);
        state.items[payload.index].name = payload.name.value;
        state.items[payload.index].description = payload.description.value;
    }
};

export default itemMutations;
