import itemMutations from "./items";
import stockMutations from "./stocks";
import serviceMutations from "./services";
import salesMutations from "./sales";

const mutations = {
    ...itemMutations,
    ...stockMutations,
    ...serviceMutations,
    ...salesMutations
};

export default mutations;
