import Vue from "vue";
import Vuex from "vuex";

import mutations from "./mutations/index";
import actions from "./actions/index";
import getters from "./getters/index";

Vue.use(Vuex);

const store = new Vuex.Store({
    modules: {},

    state: {
        items: [],
        stocks: [],
        services: [],
        sales: []
    },
    actions,
    getters,
    mutations
});

export default store;
