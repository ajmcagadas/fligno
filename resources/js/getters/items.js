const itemGetters = {
    enabledItems: state => {
        return state.items.filter(item => item.status);
    }
};

export default itemGetters;
