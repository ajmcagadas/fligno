const serviceGetters = {
    enabledServices: state => {
        return state.services.filter(service => service.status);
    }
};

export default serviceGetters;
