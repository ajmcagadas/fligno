import itemGetters from "./items";
import serviceGetters from "./services";
import saleGetters from "./sales";

const getters = {
    ...itemGetters,
    ...serviceGetters,
    ...saleGetters
};

export default getters;
