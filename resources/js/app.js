/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

import VueRouter from "vue-router";
import Vuex from "vuex";
import routes from "./routes";
import store from "./store";
import VueSweetalert2 from "vue-sweetalert2";
import vSelect from "vue-select";

/** Views */
import App from "./views/App";

Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(VueSweetalert2);

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component("Sidebar", require("./components/Sidebar.vue").default);
Vue.component("AddItemForm", require("./components/AddItemForm.vue").default);
Vue.component("EditItemForm", require("./components/EditItemForm.vue").default);
Vue.component("ItemList", require("./components/ItemList.vue").default);
Vue.component("AddStockForm", require("./components/AddStockForm.vue").default);
Vue.component("StockList", require("./components/StockList.vue").default);
Vue.component("ServiceList", require("./components/ServiceList.vue").default);
Vue.component(
    "AddServiceForm",
    require("./components/AddServiceForm.vue").default
);
Vue.component(
    "EditServiceForm",
    require("./components/EditServiceForm.vue").default
);
Vue.component("SalesForm", require("./components/SalesForm.vue").default);
Vue.component("UserList", require("./components/UserList.vue").default);

Vue.component("v-select", vSelect);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
const router = new VueRouter(routes);
const app = new Vue({
    el: "#app",
    components: { App },
    router,
    store
});
