import Home from "./views/Home";
import Items from "./views/Items";
import Stocks from "./views/Stocks";
import Services from "./views/Services";
import Users from "./views/Users";

const routes = {
    mode: "history",
    routes: [
        {
            path: "/home",
            name: "home",
            component: Home
        },
        {
            path: "/items",
            name: "items",
            component: Items
        },
        {
            path: "/items/:id/stocks",
            name: "stocks",
            component: Stocks
        },
        {
            path: "/services",
            name: "services",
            component: Services
        },
        {
            path: "/users",
            name: "users",
            component: Users
        }
    ]
};

export default routes;
