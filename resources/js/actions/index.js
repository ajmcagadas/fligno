import itemActions from "./items";
import stockActions from "./stocks";
import serviceActions from "./services";
import saleActions from "./sales";

const actions = {
    ...itemActions,
    ...stockActions,
    ...serviceActions,
    ...saleActions
};

export default actions;
