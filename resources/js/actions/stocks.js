const stockActions = {
    setStocks(context, payload) {
        context.commit("SET_STOCKS", payload);
    },
    addStock(context, payload) {
        console.log("ACTIONS PAYLOAD: ", payload);
        context.commit("ADD_STOCK", payload);
    },
    removeStock(context, payload) {
        context.commit("REMOVE_STOCK", payload);
    }
};

export default stockActions;
