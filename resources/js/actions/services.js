const serviceActions = {
    setServices(context, payload) {
        context.commit("SET_SERVICES", payload);
    },
    addService(context, payload) {
        console.log("ACTIONS PAYLOAD: ", payload);
        context.commit("ADD_SERVICE", payload);
    },
    removeService(context, payload) {
        context.commit("REMOVE_SERVICE", payload);
    },
    updateServiceStatus(context, payload) {
        context.commit("UPDATE_SERVICE_STATUS", payload);
    },
    updateServiceDetails(context, payload) {
        context.commit("UPDATE_SERVICE_DETAILS", payload);
    }
};

export default serviceActions;
