const itemActions = {
    setItems(context, payload) {
        context.commit("SET_ITEMS", payload);
    },
    addItem(context, payload) {
        context.commit("ADD_ITEM", payload);
    },
    removeItem(context, payload) {
        context.commit("REMOVE_ITEM", payload);
    },
    updateItemStatus(context, payload) {
        context.commit("UPDATE_ITEM_STATUS", payload);
    },
    updateItemDetails(context, payload) {
        context.commit("UPDATE_ITEM_DETAILS", payload);
    },
};

export default itemActions;
