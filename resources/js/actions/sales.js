const saleActions = {
    setSales(context, payload) {
        context.commit("SET_SALES", payload);
    },
    addSale(context, payload) {
        context.commit("ADD_SALE", payload);
    }
    // removeService(context, payload) {
    //     context.commit("REMOVE_SERVICE", payload);
    // },
    // updateServiceStatus(context, payload) {
    //     context.commit("UPDATE_SERVICE_STATUS", payload);
    // },
    // updateServiceDetails(context, payload) {
    //     context.commit("UPDATE_SERVICE_DETAILS", payload);
    // }
};

export default saleActions;
