<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::middleware(['auth'])->group(function() {

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/items', 'ItemController@get')->middleware('isAjax');
    Route::get('/items/{id}/stocks', 'StockController@get')->where(['id' => '[0-9]+'])->middleware('isAjax');;
    Route::get('/services', 'ServiceController@get')->middleware('isAjax');;

    Route::middleware(['isAdmin', 'isAjax'])->group(function() {

        Route::post('/item/add', 'ItemController@add');
        Route::delete('/item/delete', 'ItemController@delete');
        Route::put('/item/update-status', 'ItemController@updateStatus');
        Route::put('/item/update-details', 'ItemController@updateDetails');


        Route::post('/stock/add', 'StockController@add');
        Route::delete('/stock/delete', 'StockController@delete');


        Route::post('/services/add', 'ServiceController@add');
        Route::delete('/services/delete', 'ServiceController@delete');
        Route::put('/services/update-status', 'ServiceController@updateStatus');
        Route::put('/services/update-details', 'ServiceController@updateDetails');

        Route::get('/users', 'UserController@get');

    });

    Route::get('/sales', 'SaleController@get');
    Route::post('/sales/add', 'SaleController@add');


});